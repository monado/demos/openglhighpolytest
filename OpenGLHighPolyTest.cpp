/** @file
    @brief Example program that renders a scene that has lots
           of polygons.  This can be used to do speed tests on various
           cards, or regression tests on new versions.

           Originally written for OSVR, ported to OpenXR.

    @date 2015

    @author
    Russ Taylor <russ@sensics.com>
    <http://sensics.com/osvr>

    @author
    Christoph Haag <christoph.haag@collabora.com>
    <http://collabora.com>
*/

// Copyright 2015 Sensics, Inc.
// Copyright 2020 Collabora, Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


// Library/third-party includes
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#endif

#include <GL/glew.h>

// Standard includes
#include <iostream>
#include <string>
#include <stdlib.h> // For exit()
#include <vector>
#include <cmath>
#include <array>
#include <random>

#ifdef __linux__
#include <sys/time.h>
#define HAVE_TIMING
#endif


#ifdef __linux__
#include <X11/Xlib.h>
#include <GL/glxew.h>

#define GLFW_EXPOSE_NATIVE_X11
#define GLFW_EXPOSE_NATIVE_GLX

#define XR_USE_PLATFORM_XLIB
#elif defined(_WIN32)
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL

#define XR_USE_PLATFORM_WIN32
// needed by an extension we don't use :(
#include <Unknwn.h>
#endif
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

#define XR_USE_GRAPHICS_API_OPENGL
#include "openxr/openxr.h"
#include "openxr/openxr_platform.h"
#include <string.h>

static std::random_device rand_dev;
static std::mt19937 random_generator(rand_dev());
static std::uniform_real_distribution<float> distribution(0.f, 1.f);


// normally you'd load the shaders from a file, but in this case, let's
// just keep things simple and load from memory.
static const GLchar *vertexShader =
    "#version 330 core\n"
    "layout(location = 0) in vec3 position;\n"
    "layout(location = 1) in vec3 vertexColor;\n"
    "out vec3 fragmentColor;\n"
    "uniform mat4 modelView;\n"
    "uniform mat4 projection;\n"
    "void main()\n"
    "{\n"
    "   gl_Position = projection * modelView * vec4(position,1);\n"
    "   fragmentColor = vertexColor;\n"
    "}\n";

static const GLchar *fragmentShader =
    "#version 330 core\n"
    "in vec3 fragmentColor;\n"
    "out vec3 color;\n"
    "void main()\n"
    "{\n"
    "    color = fragmentColor;\n"
    "}\n";

class SampleShader
{
public:
	SampleShader() {}

	~SampleShader()
	{
		if (initialized) {
			glDeleteProgram(programId);
		}
	}

	void
	init()
	{
		if (!initialized) {
			GLuint vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
			GLuint fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);

			// vertex shader
			glShaderSource(vertexShaderId, 1, &vertexShader, NULL);
			glCompileShader(vertexShaderId);
			checkShaderError(vertexShaderId, "Vertex shader compilation failed.");

			// fragment shader
			glShaderSource(fragmentShaderId, 1, &fragmentShader, NULL);
			glCompileShader(fragmentShaderId);
			checkShaderError(fragmentShaderId, "Fragment shader compilation failed.");

			// linking program
			programId = glCreateProgram();
			glAttachShader(programId, vertexShaderId);
			glAttachShader(programId, fragmentShaderId);
			glLinkProgram(programId);
			checkProgramError(programId, "Shader program link failed.");

			// once linked into a program, we no longer need the shaders.
			glDeleteShader(vertexShaderId);
			glDeleteShader(fragmentShaderId);

			projectionUniformId = glGetUniformLocation(programId, "projection");
			modelViewUniformId = glGetUniformLocation(programId, "modelView");
			initialized = true;
		}
	}

	void
	useProgram(const GLdouble projection[], const GLdouble modelView[])
	{
		init();
		glUseProgram(programId);
		GLfloat projectionf[16];
		GLfloat modelViewf[16];
		convertMatrix(projection, projectionf);
		convertMatrix(modelView, modelViewf);
		glUniformMatrix4fv(projectionUniformId, 1, GL_FALSE, projectionf);
		glUniformMatrix4fv(modelViewUniformId, 1, GL_FALSE, modelViewf);
	}

private:
	SampleShader(const SampleShader &) = delete;
	SampleShader &
	operator=(const SampleShader &) = delete;
	bool initialized = false;
	GLuint programId = 0;
	GLuint projectionUniformId = 0;
	GLuint modelViewUniformId = 0;

	void
	checkShaderError(GLuint shaderId, const std::string &exceptionMsg)
	{
		GLint result = GL_FALSE;
		int infoLength = 0;
		glGetShaderiv(shaderId, GL_COMPILE_STATUS, &result);
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLength);
		if (result == GL_FALSE) {
			std::vector<GLchar> errorMessage(infoLength + 1);
			glGetProgramInfoLog(programId, infoLength, NULL, &errorMessage[0]);
			std::cerr << errorMessage.data() << std::endl;
			throw std::runtime_error(exceptionMsg);
		}
	}

	void
	checkProgramError(GLuint progId, const std::string &exceptionMsg)
	{
		GLint result = GL_FALSE;
		int infoLength = 0;
		glGetProgramiv(progId, GL_LINK_STATUS, &result);
		glGetProgramiv(progId, GL_INFO_LOG_LENGTH, &infoLength);
		if (result == GL_FALSE) {
			std::vector<GLchar> errorMessage(infoLength + 1);
			glGetProgramInfoLog(progId, infoLength, NULL, &errorMessage[0]);
			std::cerr << errorMessage.data() << std::endl;
			throw std::runtime_error(exceptionMsg);
		}
	}

	void
	convertMatrix(const GLdouble source[], GLfloat dest_out[])
	{
		if (nullptr == source || nullptr == dest_out) {
			throw std::logic_error("source and dest_out must be non-null.");
		}
		for (int i = 0; i < 16; i++) {
			dest_out[i] = (GLfloat)source[i];
		}
	}
};
static SampleShader sampleShader;

class MeshCube
{
public:
	MeshCube(GLfloat scale, size_t numTriangles = 6 * 2 * 15 * 15)
	{
		// Figure out how many quads we have per edge.  There
		// is a minimum of 1.
		size_t numQuads = numTriangles / 2;
		size_t numQuadsPerFace = numQuads / 6;
		size_t numQuadsPerEdge = static_cast<size_t>(sqrt(numQuadsPerFace));
		if (numQuadsPerEdge < 1) {
			numQuadsPerEdge = 1;
		}

		// Construct a white square with the specified number of
		// quads as the +Z face of the cube.  We'll copy this and
		// then multiply by the correct face color, and we'll
		// adjust the coordinates by rotation to match each face.
		std::vector<GLfloat> whiteBufferData;
		std::vector<GLfloat> faceBufferData;
		for (size_t i = 0; i < numQuadsPerEdge; i++) {
			for (size_t j = 0; j < numQuadsPerEdge; j++) {

				// Modulate the color of each quad by a random luminance,
				// leaving all vertices the same color.
				GLfloat color = distribution(random_generator);
				const size_t numTris = 2;
				const size_t numColors = 3;
				const size_t numVerts = 3;
				for (size_t c = 0; c < numColors * numTris * numVerts; c++) {
					whiteBufferData.push_back(color);
				}

				// Send the two triangles that make up this quad, where the
				// quad covers the appropriate fraction of the face from
				// -scale to scale in X and Y.
				GLfloat Z = scale;
				GLfloat minX = -scale + i * (2 * scale) / numQuadsPerEdge;
				GLfloat maxX = -scale + (i + 1) * (2 * scale) / numQuadsPerEdge;
				GLfloat minY = -scale + j * (2 * scale) / numQuadsPerEdge;
				GLfloat maxY = -scale + (j + 1) * (2 * scale) / numQuadsPerEdge;
				faceBufferData.push_back(minX);
				faceBufferData.push_back(maxY);
				faceBufferData.push_back(Z);

				faceBufferData.push_back(minX);
				faceBufferData.push_back(minY);
				faceBufferData.push_back(Z);

				faceBufferData.push_back(maxX);
				faceBufferData.push_back(minY);
				faceBufferData.push_back(Z);

				faceBufferData.push_back(maxX);
				faceBufferData.push_back(maxY);
				faceBufferData.push_back(Z);

				faceBufferData.push_back(minX);
				faceBufferData.push_back(maxY);
				faceBufferData.push_back(Z);

				faceBufferData.push_back(maxX);
				faceBufferData.push_back(minY);
				faceBufferData.push_back(Z);
			}
		}

		// Make a copy of the vertices for each face, then modulate
		// the color by the face color and rotate the coordinates to
		// put them on the correct cube face.

		// +Z is blue and is in the same location as the original
		// faces.
		{
			std::array<GLfloat, 3> modColor = {0.0, 0.0, 1.0};
			std::vector<GLfloat> myBufferData =
			    colorModulate(whiteBufferData, modColor);

			// X = X, Y = Y, Z = Z
			std::array<GLfloat, 3> scales = {1.0f, 1.0f, 1.0f};
			std::array<size_t, 3> indices = {0, 1, 2};
			std::vector<GLfloat> myFaceBufferData =
			    vertexRotate(faceBufferData, indices, scales);

			// Catenate the colors onto the end of the
			// color buffer.
			colorBufferData.insert(colorBufferData.end(), myBufferData.begin(),
			                       myBufferData.end());

			// Catenate the vertices onto the end of the
			// vertex buffer.
			vertexBufferData.insert(vertexBufferData.end(), myFaceBufferData.begin(),
			                        myFaceBufferData.end());
		}

		// -Z is cyan and is in the opposite size from the
		// original face (mirror all 3).
		{
			std::array<GLfloat, 3> modColor = {0.0, 1.0, 1.0};
			std::vector<GLfloat> myBufferData =
			    colorModulate(whiteBufferData, modColor);

			// X = -X, Y = -Y, Z = -Z
			std::array<GLfloat, 3> scales = {-1.0f, -1.0f, -1.0f};
			std::array<size_t, 3> indices = {0, 1, 2};
			std::vector<GLfloat> myFaceBufferData =
			    vertexRotate(faceBufferData, indices, scales);

			// Catenate the colors onto the end of the
			// color buffer.
			colorBufferData.insert(colorBufferData.end(), myBufferData.begin(),
			                       myBufferData.end());

			// Catenate the vertices onto the end of the
			// vertex buffer.
			vertexBufferData.insert(vertexBufferData.end(), myFaceBufferData.begin(),
			                        myFaceBufferData.end());
		}

		// +X is red and is rotated -90 degrees from the original
		// around Y.
		{
			std::array<GLfloat, 3> modColor = {1.0, 0.0, 0.0};
			std::vector<GLfloat> myBufferData =
			    colorModulate(whiteBufferData, modColor);

			// X = Z, Y = Y, Z = -X
			std::array<GLfloat, 3> scales = {1.0f, 1.0f, -1.0f};
			std::array<size_t, 3> indices = {2, 1, 0};
			std::vector<GLfloat> myFaceBufferData =
			    vertexRotate(faceBufferData, indices, scales);

			// Catenate the colors onto the end of the
			// color buffer.
			colorBufferData.insert(colorBufferData.end(), myBufferData.begin(),
			                       myBufferData.end());

			// Catenate the vertices onto the end of the
			// vertex buffer.
			vertexBufferData.insert(vertexBufferData.end(), myFaceBufferData.begin(),
			                        myFaceBufferData.end());
		}

		// -X is magenta and is rotated 90 degrees from the original
		// around Y.
		{
			std::array<GLfloat, 3> modColor = {1.0, 0.0, 1.0};
			std::vector<GLfloat> myBufferData =
			    colorModulate(whiteBufferData, modColor);

			// X = -Z, Y = Y, Z = X
			std::array<GLfloat, 3> scales = {-1.0f, 1.0f, 1.0f};
			std::array<size_t, 3> indices = {2, 1, 0};
			std::vector<GLfloat> myFaceBufferData =
			    vertexRotate(faceBufferData, indices, scales);

			// Catenate the colors onto the end of the
			// color buffer.
			colorBufferData.insert(colorBufferData.end(), myBufferData.begin(),
			                       myBufferData.end());

			// Catenate the vertices onto the end of the
			// vertex buffer.
			vertexBufferData.insert(vertexBufferData.end(), myFaceBufferData.begin(),
			                        myFaceBufferData.end());
		}

		// +Y is green and is rotated -90 degrees from the original
		// around X.
		{
			std::array<GLfloat, 3> modColor = {0.0, 1.0, 0.0};
			std::vector<GLfloat> myBufferData =
			    colorModulate(whiteBufferData, modColor);

			// X = X, Y = Z, Z = -Y
			std::array<GLfloat, 3> scales = {1.0f, 1.0f, -1.0f};
			std::array<size_t, 3> indices = {0, 2, 1};
			std::vector<GLfloat> myFaceBufferData =
			    vertexRotate(faceBufferData, indices, scales);

			// Catenate the colors onto the end of the
			// color buffer.
			colorBufferData.insert(colorBufferData.end(), myBufferData.begin(),
			                       myBufferData.end());

			// Catenate the vertices onto the end of the
			// vertex buffer.
			vertexBufferData.insert(vertexBufferData.end(), myFaceBufferData.begin(),
			                        myFaceBufferData.end());
		}

		// -Y is yellow and is rotated 90 degrees from the original
		// around X.
		{
			std::array<GLfloat, 3> modColor = {1.0, 1.0, 0.0};
			std::vector<GLfloat> myBufferData =
			    colorModulate(whiteBufferData, modColor);

			// X = X, Y = -Z, Z = Y
			std::array<GLfloat, 3> scales = {1.0f, -1.0f, 1.0f};
			std::array<size_t, 3> indices = {0, 2, 1};
			std::vector<GLfloat> myFaceBufferData =
			    vertexRotate(faceBufferData, indices, scales);

			// Catenate the colors onto the end of the
			// color buffer.
			colorBufferData.insert(colorBufferData.end(), myBufferData.begin(),
			                       myBufferData.end());

			// Catenate the vertices onto the end of the
			// vertex buffer.
			vertexBufferData.insert(vertexBufferData.end(), myFaceBufferData.begin(),
			                        myFaceBufferData.end());
		}
	}

	~MeshCube()
	{
		if (initialized) {
			glDeleteBuffers(1, &vertexBuffer);
			glDeleteVertexArrays(1, &vertexArrayId);
		}
	}

	void
	init()
	{
		if (!initialized) {
			// Vertex buffer
			glGenBuffers(1, &vertexBuffer);
			glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
			glBufferData(GL_ARRAY_BUFFER,
			             sizeof(vertexBufferData[0]) * vertexBufferData.size(),
			             &vertexBufferData[0], GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			// Color buffer
			glGenBuffers(1, &colorBuffer);
			glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
			glBufferData(GL_ARRAY_BUFFER,
			             sizeof(colorBufferData[0]) * colorBufferData.size(),
			             &colorBufferData[0], GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			// Vertex array object
			glGenVertexArrays(1, &vertexArrayId);
			glBindVertexArray(vertexArrayId);
			{
				// color
				glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
				glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid *)0);

				// VBO
				glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid *)0);

				glEnableVertexAttribArray(0);
				glEnableVertexAttribArray(1);
			}
			glBindVertexArray(0);
			initialized = true;
		}
	}

	void
	draw(const GLdouble projection[], const GLdouble modelView[])
	{
		init();

		sampleShader.useProgram(projection, modelView);

		glBindVertexArray(vertexArrayId);
		{
			glDrawArrays(GL_TRIANGLES, 0,
			             static_cast<GLsizei>(vertexBufferData.size()));
		}
		glBindVertexArray(0);
	}

private:
	MeshCube(const MeshCube &) = delete;
	MeshCube &
	operator=(const MeshCube &) = delete;
	bool initialized = false;
	GLuint colorBuffer = 0;
	GLuint vertexBuffer = 0;
	GLuint vertexArrayId = 0;
	std::vector<GLfloat> colorBufferData;
	std::vector<GLfloat> vertexBufferData;

	// Multiply each triple of colors by the specified color.
	std::vector<GLfloat>
	colorModulate(std::vector<GLfloat> const &inVec, std::array<GLfloat, 3> const &clr)
	{
		std::vector<GLfloat> out;
		size_t elements = inVec.size() / 3;
		if (elements * 3 != inVec.size()) {
			// We don't have an even multiple of 3 elements, so bail.
			return out;
		}
		out = inVec;
		for (size_t i = 0; i < elements; i++) {
			for (size_t c = 0; c < 3; c++) {
				out[3 * i + c] *= clr[c];
			}
		}
		return out;
	}

	// Swizzle each triple of coordinates by the specified
	// index and then multiply by the specified scale.  This
	// lets us implement a poor-man's rotation matrix, where
	// we pick which element (0-2) and which polarity (-1 or
	// 1) to use.
	std::vector<GLfloat>
	vertexRotate(std::vector<GLfloat> const &inVec,
	             std::array<size_t, 3> const &indices,
	             std::array<GLfloat, 3> const &scales)
	{
		std::vector<GLfloat> out;
		size_t elements = inVec.size() / 3;
		if (elements * 3 != inVec.size()) {
			// We don't have an even multiple of 3 elements, so bail.
			return out;
		}
		out.resize(inVec.size());
		for (size_t i = 0; i < elements; i++) {
			for (size_t p = 0; p < 3; p++) {
				out[3 * i + p] = inVec[3 * i + indices[p]] * scales[p];
			}
		}
		return out;
	}
};

static MeshCube *roomCube = nullptr;

// Set to true when it is time for the application to quit.
// Handlers below that set it to true when the user causes
// any of a variety of events so that we shut down the system
// cleanly.  This only works on Windows.
static bool quit = false;

#ifdef _WIN32
// Note: On Windows, this runs in a different thread from
// the main application.
static BOOL
CtrlHandler(DWORD fdwCtrlType)
{
	switch (fdwCtrlType) {
	// Handle the CTRL-C signal.
	case CTRL_C_EVENT:
	// CTRL-CLOSE: confirm that the user wants to exit.
	case CTRL_CLOSE_EVENT:
	case CTRL_BREAK_EVENT:
	case CTRL_LOGOFF_EVENT:
	case CTRL_SHUTDOWN_EVENT: quit = true; return TRUE;
	default: return FALSE;
	}
}
#endif

// Callbacks to draw things in world space.
void
DrawWorld(float *projectionMatrix, float *viewMatrix)
{

	GLdouble projectionGL[16];
	for (int i = 0; i < 16; i++)
		projectionGL[i] = (GLdouble)projectionMatrix[i];

	GLdouble viewGL[16];
	for (int i = 0; i < 16; i++)
		viewGL[i] = (GLdouble)viewMatrix[i];

	/// Draw a cube with a 5-meter radius as the room we are floating in.
	roomCube->draw(projectionGL, viewGL);
}

void
error_callback(int /* error */, const char *description)
{
	fprintf(stderr, "Error: %s\n", description);
}

bool window_visible = false;
GLFWwindow *opengl_window;

#define CHECK(cmd)                                                                                 \
	{                                                                                          \
		XrResult result = (cmd);                                                           \
		if (!XR_SUCCEEDED(result)) {                                                       \
			char resultString[XR_MAX_RESULT_STRING_SIZE];                              \
			xrResultToString(self->instance, result, resultString);                    \
			printf("Error in line %d: [%s]\n", __LINE__, resultString);                \
			return false;                                                              \
		}                                                                                  \
	}

struct oxr
{
	bool quit;
	XrInstance instance;
	XrSession session;
	XrSpace play_space;
#ifdef __linux__
	XrGraphicsBindingOpenGLXlibKHR graphics_binding_gl;
#elif defined(_WIN32)
	XrGraphicsBindingOpenGLWin32KHR graphics_binding_gl;
#endif
	XrSwapchainImageOpenGLKHR **images;
	XrSwapchain *swapchains;
	uint32_t view_count;
	XrViewConfigurationView *configuration_views;
	XrCompositionLayerProjection projectionLayer;
	XrView *views;
	XrCompositionLayerProjectionView *projection_views;
	XrFrameState frameState;
	XrSessionState state;
	uint32_t *swapchainLength;

	// helper to render opengl textures
	GLuint **framebuffers;
};

bool
init_xr(struct oxr *self)
{
	const char *const enabledExtensions[] = {XR_KHR_OPENGL_ENABLE_EXTENSION_NAME};

	XrInstanceCreateInfo instanceCreateInfo = {
	    .type = XR_TYPE_INSTANCE_CREATE_INFO,
	    .next = NULL,
	    .createFlags = 0,
	    .applicationInfo =
	        {
	            .applicationName = {},
	            .applicationVersion = 1,
	            .engineName = {},
	            .engineVersion = 0,
	            .apiVersion = XR_CURRENT_API_VERSION,
	        },
	    .enabledExtensionCount = sizeof(enabledExtensions) / sizeof(enabledExtensions[0]),
	    .enabledExtensionNames = enabledExtensions,
	};
	strncpy(instanceCreateInfo.applicationInfo.applicationName, "OpenGLHighPolyTest",
	        XR_MAX_APPLICATION_NAME_SIZE);

	XrInstance instance;
	CHECK(xrCreateInstance(&instanceCreateInfo, &instance));

	self->instance = instance; // just to make CHECK macro work

	XrSystemGetInfo systemGetInfo = {.type = XR_TYPE_SYSTEM_GET_INFO,
	                                 .formFactor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY};

	XrSystemId systemId;
	CHECK(xrGetSystem(instance, &systemGetInfo, &systemId));

	uint32_t viewConfigurationCount;
	CHECK(xrEnumerateViewConfigurations(instance, systemId, 0, &viewConfigurationCount, NULL));

	XrViewConfigurationType stereoViewConfigType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;

	uint32_t view_count;
	CHECK(xrEnumerateViewConfigurationViews(instance, systemId, stereoViewConfigType, 0,
	                                        &view_count, NULL));
	XrViewConfigurationView *configuration_views =
	    (XrViewConfigurationView *)malloc(sizeof(XrViewConfigurationView) * view_count);
	for (uint32_t i = 0; i < view_count; i++) {
		configuration_views[i].type = XR_TYPE_VIEW_CONFIGURATION_VIEW;
		configuration_views[i].next = NULL;
	}

	CHECK(xrEnumerateViewConfigurationViews(instance, systemId, stereoViewConfigType,
	                                        view_count, &view_count, configuration_views));

	XrGraphicsRequirementsOpenGLKHR gl_reqs = {.type =
	                                               XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_KHR};

	PFN_xrGetOpenGLGraphicsRequirementsKHR pfnGetOpenGLGraphicsRequirementsKHR = NULL;
	CHECK(xrGetInstanceProcAddr(instance, "xrGetOpenGLGraphicsRequirementsKHR",
	                            (PFN_xrVoidFunction *)&pfnGetOpenGLGraphicsRequirementsKHR));
	CHECK(pfnGetOpenGLGraphicsRequirementsKHR(instance, systemId, &gl_reqs));

#ifdef __linux__
	XrGraphicsBindingOpenGLXlibKHR graphics_binding_gl = {
	    .type = XR_TYPE_GRAPHICS_BINDING_OPENGL_XLIB_KHR,
	    .next = NULL,
	    .xDisplay = glfwGetX11Display(),
	    .visualid = 0,
	    .glxFBConfig = 0,
	    .glxDrawable = glfwGetGLXWindow(opengl_window),
	    .glxContext = glfwGetGLXContext(opengl_window),
	};
#elif defined(_WIN32)
	XrGraphicsBindingOpenGLWin32KHR graphics_binding_gl = {
	    .type = XR_TYPE_GRAPHICS_BINDING_OPENGL_WIN32_KHR,
	    .next = NULL,
	    //! @todo this is wrong presumably
	    .hDC = NULL,
	    .hGLRC = glfwGetWGLContext(opengl_window),
	};
#endif

	XrSessionCreateInfo session_create_info = {.type = XR_TYPE_SESSION_CREATE_INFO,
	                                           .next = &graphics_binding_gl,
	                                           .systemId = systemId};

	XrSession session;
	CHECK(xrCreateSession(instance, &session_create_info, &session));

	XrPosef identityPose = {.orientation = {.x = 0, .y = 0, .z = 0, .w = 1.0},
	                        .position = {.x = 0, .y = 0, .z = 0}};

	XrReferenceSpaceCreateInfo playSpaceCreateInfo = {
	    .type = XR_TYPE_REFERENCE_SPACE_CREATE_INFO,
	    .referenceSpaceType = XR_REFERENCE_SPACE_TYPE_LOCAL,
	    .poseInReferenceSpace = identityPose};

	XrSpace play_space;
	CHECK(xrCreateReferenceSpace(session, &playSpaceCreateInfo, &play_space));

	XrSessionBeginInfo sessionBeginInfo = {.type = XR_TYPE_SESSION_BEGIN_INFO,
	                                       .primaryViewConfigurationType =
	                                           stereoViewConfigType};
	CHECK(xrBeginSession(session, &sessionBeginInfo));

	uint32_t swapchainFormatCount;
	CHECK(xrEnumerateSwapchainFormats(session, 0, &swapchainFormatCount, NULL));

	int64_t *swapchainFormats = (int64_t *)malloc(sizeof(int64_t) * swapchainFormatCount);
	CHECK(xrEnumerateSwapchainFormats(session, swapchainFormatCount, &swapchainFormatCount,
	                                  swapchainFormats));

	int64_t swapchainFormatToUse = swapchainFormats[0];

	XrSwapchain *swapchains = (XrSwapchain *)malloc(sizeof(XrSwapchain) * view_count);
	uint32_t *swapchainLength = (uint32_t *)malloc(sizeof(uint32_t) * view_count);

	XrSwapchainImageOpenGLKHR **images =
	    (XrSwapchainImageOpenGLKHR **)malloc(sizeof(XrSwapchainImageOpenGLKHR *) * view_count);
	for (uint32_t i = 0; i < view_count; i++) {
		XrSwapchainCreateInfo swapchainCreateInfo = {
		    .type = XR_TYPE_SWAPCHAIN_CREATE_INFO,
		    .next = NULL,
		    .createFlags = 0,
		    .usageFlags =
		        XR_SWAPCHAIN_USAGE_SAMPLED_BIT | XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT,
		    .format = swapchainFormatToUse,
		    .sampleCount = 1,
		    .width = configuration_views[i].recommendedImageRectWidth,
		    .height = configuration_views[i].recommendedImageRectHeight,
		    .faceCount = 1,
		    .arraySize = 1,
		    .mipCount = 1,
		};

		CHECK(xrCreateSwapchain(session, &swapchainCreateInfo, &swapchains[i]));
		CHECK(xrEnumerateSwapchainImages(swapchains[i], 0, &swapchainLength[i], NULL));

		images[i] = (XrSwapchainImageOpenGLKHR *)malloc(sizeof(XrSwapchainImageOpenGLKHR) *
		                                                swapchainLength[i]);
		for (uint32_t j = 0; j < swapchainLength[i]; j++) {
			images[i][j].type = XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_KHR;
			images[i][j].next = NULL;
		}
		CHECK(xrEnumerateSwapchainImages(swapchains[i], swapchainLength[i],
		                                 &swapchainLength[i],
		                                 (XrSwapchainImageBaseHeader *)images[i]));
	}

	XrCompositionLayerProjection projectionLayer = {
	    .type = XR_TYPE_COMPOSITION_LAYER_PROJECTION,
	    .layerFlags = 0,
	    .space = play_space,
	    .viewCount = view_count,
	    .views = NULL,
	};

	XrView *views = (XrView *)malloc(sizeof(XrView) * view_count);
	for (uint32_t i = 0; i < view_count; i++) {
		views[i].type = XR_TYPE_VIEW;
		views[i].next = NULL;
	};

	XrCompositionLayerProjectionView *projection_views =
	    (XrCompositionLayerProjectionView *)malloc(sizeof(XrCompositionLayerProjectionView) *
	                                               view_count);
	for (uint32_t i = 0; i < view_count; i++) {
		projection_views[i].type = XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW;
		projection_views[i].next = NULL;
		projection_views[i].fov = (XrFovf) {}; // zero initialize values we don't know yet'
		projection_views[i].pose = (XrPosef) {};
		projection_views[i].subImage.swapchain = swapchains[i];
		projection_views[i].subImage.imageArrayIndex = 0;
		projection_views[i].subImage.imageRect.offset.x = 0;
		projection_views[i].subImage.imageRect.offset.y = 0;
		projection_views[i].subImage.imageRect.extent.width =
		    configuration_views[i].recommendedImageRectWidth;
		projection_views[i].subImage.imageRect.extent.height =
		    configuration_views[i].recommendedImageRectHeight;
		projectionLayer.views = projection_views;
	}


	self->instance = instance;
	self->graphics_binding_gl = graphics_binding_gl;
	self->images = images;
	self->play_space = play_space;
	self->configuration_views = configuration_views;
	self->session = session;
	self->swapchains = swapchains;
	self->view_count = view_count;
	self->projectionLayer = projectionLayer;
	self->views = views;
	self->projection_views = projection_views;
	self->state = XR_SESSION_STATE_UNKNOWN;
	self->swapchainLength = swapchainLength;
	self->quit = false;


	self->framebuffers = (GLuint **)malloc(sizeof(GLuint *) * self->view_count);
	for (uint32_t i = 0; i < self->view_count; i++) {
		self->framebuffers[i] = (GLuint *)malloc(sizeof(GLuint) * swapchainLength[i]);
		glGenFramebuffers(swapchainLength[i], self->framebuffers[i]);
	}

	return true;
}

// math code adapted from
// https://github.com/KhronosGroup/OpenXR-SDK-Source/blob/master/src/common/xr_linear.h
// Copyright (c) 2017 The Khronos Group Inc.
// Copyright (c) 2016 Oculus VR, LLC.
// SPDX-License-Identifier: Apache-2.0

typedef enum
{
	GRAPHICS_VULKAN,
	GRAPHICS_OPENGL,
	GRAPHICS_OPENGL_ES
} GraphicsAPI;

typedef struct XrMatrix4x4f
{
	float m[16];
} XrMatrix4x4f;

inline static void
XrMatrix4x4f_CreateProjectionFov(XrMatrix4x4f *result,
                                 GraphicsAPI graphicsApi,
                                 const XrFovf fov,
                                 const float nearZ,
                                 const float farZ)
{
	const float tanAngleLeft = tanf(fov.angleLeft);
	const float tanAngleRight = tanf(fov.angleRight);

	const float tanAngleDown = tanf(fov.angleDown);
	const float tanAngleUp = tanf(fov.angleUp);

	const float tanAngleWidth = tanAngleRight - tanAngleLeft;

	// Set to tanAngleDown - tanAngleUp for a clip space with positive Y
	// down (Vulkan). Set to tanAngleUp - tanAngleDown for a clip space with
	// positive Y up (OpenGL / D3D / Metal).
	const float tanAngleHeight = graphicsApi == GRAPHICS_VULKAN ? (tanAngleDown - tanAngleUp)
	                                                            : (tanAngleUp - tanAngleDown);

	// Set to nearZ for a [-1,1] Z clip space (OpenGL / OpenGL ES).
	// Set to zero for a [0,1] Z clip space (Vulkan / D3D / Metal).
	const float offsetZ =
	    (graphicsApi == GRAPHICS_OPENGL || graphicsApi == GRAPHICS_OPENGL_ES) ? nearZ : 0;

	if (farZ <= nearZ) {
		// place the far plane at infinity
		result->m[0] = 2 / tanAngleWidth;
		result->m[4] = 0;
		result->m[8] = (tanAngleRight + tanAngleLeft) / tanAngleWidth;
		result->m[12] = 0;

		result->m[1] = 0;
		result->m[5] = 2 / tanAngleHeight;
		result->m[9] = (tanAngleUp + tanAngleDown) / tanAngleHeight;
		result->m[13] = 0;

		result->m[2] = 0;
		result->m[6] = 0;
		result->m[10] = -1;
		result->m[14] = -(nearZ + offsetZ);

		result->m[3] = 0;
		result->m[7] = 0;
		result->m[11] = -1;
		result->m[15] = 0;
	} else {
		// normal projection
		result->m[0] = 2 / tanAngleWidth;
		result->m[4] = 0;
		result->m[8] = (tanAngleRight + tanAngleLeft) / tanAngleWidth;
		result->m[12] = 0;

		result->m[1] = 0;
		result->m[5] = 2 / tanAngleHeight;
		result->m[9] = (tanAngleUp + tanAngleDown) / tanAngleHeight;
		result->m[13] = 0;

		result->m[2] = 0;
		result->m[6] = 0;
		result->m[10] = -(farZ + offsetZ) / (farZ - nearZ);
		result->m[14] = -(farZ * (nearZ + offsetZ)) / (farZ - nearZ);

		result->m[3] = 0;
		result->m[7] = 0;
		result->m[11] = -1;
		result->m[15] = 0;
	}
}

inline static void
XrMatrix4x4f_CreateFromQuaternion(XrMatrix4x4f *result, const XrQuaternionf *quat)
{
	const float x2 = quat->x + quat->x;
	const float y2 = quat->y + quat->y;
	const float z2 = quat->z + quat->z;

	const float xx2 = quat->x * x2;
	const float yy2 = quat->y * y2;
	const float zz2 = quat->z * z2;

	const float yz2 = quat->y * z2;
	const float wx2 = quat->w * x2;
	const float xy2 = quat->x * y2;
	const float wz2 = quat->w * z2;
	const float xz2 = quat->x * z2;
	const float wy2 = quat->w * y2;

	result->m[0] = 1.0f - yy2 - zz2;
	result->m[1] = xy2 + wz2;
	result->m[2] = xz2 - wy2;
	result->m[3] = 0.0f;

	result->m[4] = xy2 - wz2;
	result->m[5] = 1.0f - xx2 - zz2;
	result->m[6] = yz2 + wx2;
	result->m[7] = 0.0f;

	result->m[8] = xz2 + wy2;
	result->m[9] = yz2 - wx2;
	result->m[10] = 1.0f - xx2 - yy2;
	result->m[11] = 0.0f;

	result->m[12] = 0.0f;
	result->m[13] = 0.0f;
	result->m[14] = 0.0f;
	result->m[15] = 1.0f;
}

inline static void
XrMatrix4x4f_CreateTranslation(XrMatrix4x4f *result, const float x, const float y, const float z)
{
	result->m[0] = 1.0f;
	result->m[1] = 0.0f;
	result->m[2] = 0.0f;
	result->m[3] = 0.0f;
	result->m[4] = 0.0f;
	result->m[5] = 1.0f;
	result->m[6] = 0.0f;
	result->m[7] = 0.0f;
	result->m[8] = 0.0f;
	result->m[9] = 0.0f;
	result->m[10] = 1.0f;
	result->m[11] = 0.0f;
	result->m[12] = x;
	result->m[13] = y;
	result->m[14] = z;
	result->m[15] = 1.0f;
}

inline static void
XrMatrix4x4f_Multiply(XrMatrix4x4f *result, const XrMatrix4x4f *a, const XrMatrix4x4f *b)
{
	result->m[0] =
	    a->m[0] * b->m[0] + a->m[4] * b->m[1] + a->m[8] * b->m[2] + a->m[12] * b->m[3];
	result->m[1] =
	    a->m[1] * b->m[0] + a->m[5] * b->m[1] + a->m[9] * b->m[2] + a->m[13] * b->m[3];
	result->m[2] =
	    a->m[2] * b->m[0] + a->m[6] * b->m[1] + a->m[10] * b->m[2] + a->m[14] * b->m[3];
	result->m[3] =
	    a->m[3] * b->m[0] + a->m[7] * b->m[1] + a->m[11] * b->m[2] + a->m[15] * b->m[3];

	result->m[4] =
	    a->m[0] * b->m[4] + a->m[4] * b->m[5] + a->m[8] * b->m[6] + a->m[12] * b->m[7];
	result->m[5] =
	    a->m[1] * b->m[4] + a->m[5] * b->m[5] + a->m[9] * b->m[6] + a->m[13] * b->m[7];
	result->m[6] =
	    a->m[2] * b->m[4] + a->m[6] * b->m[5] + a->m[10] * b->m[6] + a->m[14] * b->m[7];
	result->m[7] =
	    a->m[3] * b->m[4] + a->m[7] * b->m[5] + a->m[11] * b->m[6] + a->m[15] * b->m[7];

	result->m[8] =
	    a->m[0] * b->m[8] + a->m[4] * b->m[9] + a->m[8] * b->m[10] + a->m[12] * b->m[11];
	result->m[9] =
	    a->m[1] * b->m[8] + a->m[5] * b->m[9] + a->m[9] * b->m[10] + a->m[13] * b->m[11];
	result->m[10] =
	    a->m[2] * b->m[8] + a->m[6] * b->m[9] + a->m[10] * b->m[10] + a->m[14] * b->m[11];
	result->m[11] =
	    a->m[3] * b->m[8] + a->m[7] * b->m[9] + a->m[11] * b->m[10] + a->m[15] * b->m[11];

	result->m[12] =
	    a->m[0] * b->m[12] + a->m[4] * b->m[13] + a->m[8] * b->m[14] + a->m[12] * b->m[15];
	result->m[13] =
	    a->m[1] * b->m[12] + a->m[5] * b->m[13] + a->m[9] * b->m[14] + a->m[13] * b->m[15];
	result->m[14] =
	    a->m[2] * b->m[12] + a->m[6] * b->m[13] + a->m[10] * b->m[14] + a->m[14] * b->m[15];
	result->m[15] =
	    a->m[3] * b->m[12] + a->m[7] * b->m[13] + a->m[11] * b->m[14] + a->m[15] * b->m[15];
}

inline static void
XrMatrix4x4f_Invert(XrMatrix4x4f *result, const XrMatrix4x4f *src)
{
	result->m[0] = src->m[0];
	result->m[1] = src->m[4];
	result->m[2] = src->m[8];
	result->m[3] = 0.0f;
	result->m[4] = src->m[1];
	result->m[5] = src->m[5];
	result->m[6] = src->m[9];
	result->m[7] = 0.0f;
	result->m[8] = src->m[2];
	result->m[9] = src->m[6];
	result->m[10] = src->m[10];
	result->m[11] = 0.0f;
	result->m[12] = -(src->m[0] * src->m[12] + src->m[1] * src->m[13] + src->m[2] * src->m[14]);
	result->m[13] = -(src->m[4] * src->m[12] + src->m[5] * src->m[13] + src->m[6] * src->m[14]);
	result->m[14] =
	    -(src->m[8] * src->m[12] + src->m[9] * src->m[13] + src->m[10] * src->m[14]);
	result->m[15] = 1.0f;
}

inline static void
XrMatrix4x4f_CreateViewMatrix(XrMatrix4x4f *result,
                              const XrVector3f *translation,
                              const XrQuaternionf *rotation)
{

	XrMatrix4x4f rotationMatrix;
	XrMatrix4x4f_CreateFromQuaternion(&rotationMatrix, rotation);

	XrMatrix4x4f translationMatrix;
	XrMatrix4x4f_CreateTranslation(&translationMatrix, translation->x, translation->y,
	                               translation->z);

	XrMatrix4x4f viewMatrix;
	XrMatrix4x4f_Multiply(&viewMatrix, &translationMatrix, &rotationMatrix);

	XrMatrix4x4f_Invert(result, &viewMatrix);
}

bool
render_xr(struct oxr *self)
{
	XrEventDataBuffer runtimeEvent = {.type = XR_TYPE_EVENT_DATA_BUFFER, .next = NULL};
	XrResult pollResult = xrPollEvent(self->instance, &runtimeEvent);
	if (pollResult == XR_SUCCESS) {
		switch (runtimeEvent.type) {
		case XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED: {
			XrEventDataSessionStateChanged *event =
			    (XrEventDataSessionStateChanged *)&runtimeEvent;
			self->state = event->state;

			if (event->state == XR_SESSION_STATE_STOPPING ||
			    event->state == XR_SESSION_STATE_EXITING ||
			    event->state == XR_SESSION_STATE_LOSS_PENDING) {
				return false;
				break;
			}
		}
		default: break;
		}
	} else if (pollResult == XR_EVENT_UNAVAILABLE) {
	} else {
		return false;
	}

	if (self->quit) {
		xrRequestExitSession(self->session);
		return true;
	}

	self->frameState.type = XR_TYPE_FRAME_STATE;
	XrFrameWaitInfo frameWaitInfo = {.type = XR_TYPE_FRAME_WAIT_INFO};
	CHECK(xrWaitFrame(self->session, &frameWaitInfo, &self->frameState));

	XrViewLocateInfo viewLocateInfo = {.type = XR_TYPE_VIEW_LOCATE_INFO,
	                                   .viewConfigurationType =
	                                       XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO,
	                                   .displayTime = self->frameState.predictedDisplayTime,
	                                   .space = self->play_space};

	XrViewState viewState = {.type = XR_TYPE_VIEW_STATE, .next = NULL};
	uint32_t viewCountOutput;
	CHECK(xrLocateViews(self->session, &viewLocateInfo, &viewState, self->view_count,
	                    &viewCountOutput, self->views));

	for (uint32_t i = 0; i < self->view_count; i++) {
		self->projection_views[i].pose = self->views[i].pose;
		self->projection_views[i].fov = self->views[i].fov;
	}

	XrFrameBeginInfo frameBeginInfo = {.type = XR_TYPE_FRAME_BEGIN_INFO};
	CHECK(xrBeginFrame(self->session, &frameBeginInfo));

	for (uint32_t i = 0; i < self->view_count; i++) {
		XrMatrix4x4f projection;
		XrMatrix4x4f_CreateProjectionFov(&projection, GRAPHICS_OPENGL, self->views[i].fov,
		                                 0.1f, 100.f);

		XrMatrix4x4f viewMatrix;
		XrMatrix4x4f_CreateViewMatrix(&viewMatrix, &self->views[i].pose.position,
		                              &self->views[i].pose.orientation);

		XrSwapchainImageAcquireInfo swapchainImageAcquireInfo = {
		    .type = XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO, .next = NULL};
		uint32_t bufferIndex;
		CHECK(xrAcquireSwapchainImage(self->swapchains[i], &swapchainImageAcquireInfo,
		                              &bufferIndex));

		XrSwapchainImageWaitInfo swapchainImageWaitInfo = {
		    .type = XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO, .timeout = 1000};
		CHECK(xrWaitSwapchainImage(self->swapchains[i], &swapchainImageWaitInfo));

		glBindFramebuffer(GL_FRAMEBUFFER, self->framebuffers[i][bufferIndex]);
		int w = self->configuration_views[i].recommendedImageRectWidth;
		int h = self->configuration_views[i].recommendedImageRectHeight;
		glViewport(0, 0, w, h);
		glScissor(0, 0, w, h);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
		                       self->images[i][bufferIndex].image, 0);

		glClearColor((float)i, 1.f - i, 0.f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		DrawWorld(projection.m, viewMatrix.m);

		if (i == 0 && window_visible) {
			glBlitNamedFramebuffer(
			    (GLuint)self->framebuffers[i][bufferIndex], // readFramebuffer
			    (GLuint)0,                       // backbuffer     // drawFramebuffer
			    (GLint)0,                        // srcX0
			    (GLint)0,                        // srcY0
			    (GLint)w,                        // srcX1
			    (GLint)h,                        // srcY1
			    (GLint)0,                        // dstX0
			    (GLint)0,                        // dstY0
			    (GLint)w / 2,                    // dstX1
			    (GLint)h / 2,                    // dstY1
			    (GLbitfield)GL_COLOR_BUFFER_BIT, // mask
			    (GLenum)GL_LINEAR);              // filter
			glfwSwapBuffers(opengl_window);
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		XrSwapchainImageReleaseInfo swapchainImageReleaseInfo = {
		    .type = XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO};
		CHECK(xrReleaseSwapchainImage(self->swapchains[i], &swapchainImageReleaseInfo));
	}


	const XrCompositionLayerBaseHeader *submittedLayers[] = {
	    (const XrCompositionLayerBaseHeader *)&self->projectionLayer};

	bool viewTracked = (viewState.viewStateFlags & XR_VIEW_STATE_ORIENTATION_VALID_BIT) != 0 &&
	                   (viewState.viewStateFlags & XR_VIEW_STATE_ORIENTATION_TRACKED_BIT) != 0;

	const uint32_t layerCount =
	    viewTracked ? sizeof(submittedLayers) / sizeof(submittedLayers[0]) : 0;
	XrFrameEndInfo frameEndInfo = {.type = XR_TYPE_FRAME_END_INFO,
	                               .next = NULL,
	                               .displayTime = self->frameState.predictedDisplayTime,
	                               .environmentBlendMode = XR_ENVIRONMENT_BLEND_MODE_OPAQUE,
	                               .layerCount = layerCount,
	                               .layers = submittedLayers};
	CHECK(xrEndFrame(self->session, &frameEndInfo));

	return true;
}

bool
quit_xr(struct oxr *self)
{
	xrEndSession(self->session);
	xrDestroySession(self->session);
	xrDestroyInstance(self->instance);

	free(self->configuration_views);
	free(self->projection_views);
	free(self->views);
	free(self->swapchains);
	for (uint32_t i = 0; i < self->view_count; i++) {
		free(self->images[i]);
	}
	free(self->images);

	for (uint32_t i = 0; i < self->view_count; i++) {
		glDeleteFramebuffers(self->swapchainLength[i], self->framebuffers[i]);
		free(self->framebuffers[i]);
	}
	free(self->framebuffers);
	free(self->swapchainLength);

	return true;
}

bool
initGlfw()
{
	if (!glfwInit()) {
		return false;
	}

	glfwSetErrorCallback(error_callback);

	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	if (!window_visible) {
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	opengl_window = glfwCreateWindow(1024, 768, "OpenGLHighPolyTest", NULL, NULL);
	if (!opengl_window) {
		return false;
	}
	glfwMakeContextCurrent(opengl_window);
	return true;
}

void
Usage(std::string name)
{
	std::cerr << "Usage: " << name << " [TrianglesPerSide]" << std::endl;
	std::cerr << "       Default triangles per cube face = 1e3" << std::endl;

	exit(-1);
}

int
main(int argc, char *argv[])
{
	// Parse the command line
	double trianglesPerSide = 1e3;
	int realParams = 0;
	for (int i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			Usage(argv[0]);
		} else {
			switch (++realParams) {
			case 1: trianglesPerSide = atof(argv[i]); break;
			default: Usage(argv[0]);
			}
		}
	}
	if (realParams > 1) {
		Usage(argv[0]);
	}
	size_t triangles = static_cast<size_t>(trianglesPerSide * 6);
	roomCube = new MeshCube(5.0, triangles);
	std::cout << "Rendering " << trianglesPerSide << " triangles per cube face" << std::endl;
	std::cout << "Rendering " << triangles << " triangles total" << std::endl;


// Set up a handler to cause us to exit cleanly.
#ifdef _WIN32
	SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE);
#endif

	initGlfw();

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	GLenum ret = glewInit();
	if (ret != GLEW_OK) {
		std::cerr << "Failed to initialize GLEW: " << ret << std::endl;
		return -1;
	}
	// Clear any GL error that Glew caused.  Apparently on Non-Windows
	// platforms, this can cause a spurious  error 1280.
	glGetError();

	glClearColor(0, 0, 0, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	struct oxr self = {.instance = XR_NULL_HANDLE};

	if (!init_xr(&self)) {
		std::cerr << "Failed to initialize XR" << std::endl;
		return -1;
	}

#ifdef HAVE_TIMING
	// Frame timing
	size_t countFrames = 0;
	struct timeval startFrames;
	gettimeofday(&startFrames, nullptr);
#endif

	// Continue rendering until it is time to quit.
	while (true) {
		if (quit) {
			self.quit = true;
		}

		// draw world
		if (!render_xr(&self)) {
			break;
		}

#ifdef HAVE_TIMING
		// Print timing info
		struct timeval nowFrames;
		gettimeofday(&nowFrames, nullptr);
		double duration = ((double)nowFrames.tv_sec + nowFrames.tv_usec / 1000000L) -
		                  ((double)startFrames.tv_sec + startFrames.tv_usec / 1000000L);
		countFrames++;
		if (duration >= 2.0) {
			std::cout << "Rendering at " << countFrames / duration << " fps"
			          << std::endl;
			startFrames = nowFrames;
			countFrames = 0;
		}
#endif
	}

	// Close the Renderer interface cleanly.
	delete roomCube;

	quit_xr(&self);

	glfwTerminate();

	return 0;
}
